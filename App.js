import React from 'react';
import logo from './images/NetGrocers.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <center>
      <img src={logo} alt=""/>
      <form>
      <div className="login">
                <h1><u>Please login in</u></h1>
                <label htmlfor="name" htmlFor="email">Email or username</label><br/>
                <input type="text" name="email" id="email" autocomplete="off" placeholder="Enter Email or Username"></input><br/>
                <label htmlfor="password">password</label><br/>
                <input type="password" name="password" id="password" autocomplete="off" placeholder="Enter Password"></input><br/>
                <input type="submit" value="Log in"/><br/>
                <p1>----------(or)-----------</p1><br/>
                <p1>forgot password?</p1><br/>
                <p1>don't have an account?<u>sign up</u></p1>
      </div>
      </form>
      </center>
    </div>
  );
}

export default App;
